;;;; -*- coding: utf-8 -*-
;;
;; Copyright 2020 Genworks International
;;
;; This source file is part of the General-purpose Declarative
;; Language project (Gendl).
;;
;; This source file contains free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either
;; version 3 of the License, or (at your option) any later version.
;; 
;; This source file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;; 
;; You should have received a copy of the GNU Affero General Public
;; License along with this source file.  If not, see
;; <http://www.gnu.org/licenses/>.
;; 

(in-package :geysr)

(define-object viewport (base-ajax-graphics-sheet)
  
  :input-slots
  (geysr
   root-object root-object-type

   (vector-graphics-onclick? (not (or (member (the image-format) '(:svg :x3dom))
				      (eql (the digitation-mode) :select-object))))


   (empty-display-list-message nil)
   
   (color nil)
	     
   (line-thickness-selected nil)
	     
   onclick-function
	     
   (inspected-object nil)
	     
   (display-list-object-roots 
    (let (result)
      (maphash #'(lambda(root-path value)
		   (declare (ignore value))
		   (push (if (listp root-path)
			     (the root-object (follow-root-path root-path))
			     root-path) result))
	       (the view-object-roots-root-paths-hash))
      (when (some #'(lambda(obj)(typep obj 'base-object)) result) 
	(nreverse result))))
	     
	     
   (display-list-objects 
    (let (result)
      (the recompute?)
      (maphash #'(lambda(root-path value)
		   (declare (ignore value))
		   (push (the root-object (follow-root-path root-path)) result))
	       (the view-objects-root-paths-hash))
      (when (some #'(lambda(obj) (typep obj 'base-object)) result) (nreverse result))))
	     
   (image-format-default :svg))

  :computed-slots
  (
    
   (empty? (not (or (the display-list-object-roots)
		    (the display-list-objects))))
   
   (recompute? nil :settable)
   
   (respondent (the geysr))
   
   ;;
   ;; FLAG -- consider changing back to a list so we can preserve ordering.
   ;;
   (view-object-roots-root-paths-hash (glisp:make-sans-value-equalp-hash-table) :settable)
   
   (view-objects-root-paths-hash (glisp:make-sans-value-equalp-hash-table) :settable)

   (display-controls-root-path-hash (make-hash-table :test #'equalp) :settable)
   
   (display-controls-hash (let ((ht (make-hash-table)))
                            (maphash #'(lambda(root-path plist)
                                         (setf (gethash (the root-object (follow-root-path root-path)) ht) plist))
                                     (the display-controls-root-path-hash)) ht))
   
   
   ;;(operations-list nil :settable)


   (popup-menu-text

    (case (the image-format-selector value)
      ((:x3dom :svg :png :jpeg)
       (format nil
	       "
console.log ('running popup-menu-text...');     

function getPosition(e) {
var posx = 0;
var posy = 0;

if (!e) var e = window.event;

if (e.pageX || e.pageY) {
   posx = e.pageX;
   posy = e.pageY;
 } 
  else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + 
                       document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + 
                       document.documentElement.scrollTop;
  }

  return {
    x: posx,
    y: posy
  }
}


var menuPosition;
var menuPositionX;
var menuPositionY;


var can = document.getElementById('x3dom-x3dom-1-canvas');
var  domid = document.getElementById('~a');

var obj = can || domid;


var menuState = 0;
var active = 'popup-menu-visible';
var vmenu = document.getElementById('~a');


function positionMenu(e) {
 
  let popup = document.querySelector('.popup-menu');
  popup.classList.remove('mode-left');
  popup.classList.remove('mode-top');
  menuPosition = getPosition(e);
  let windowWidth = window.innerWidth;
  let windowHeight = window.innerHeight;
  let positionX = menuPosition.x;
  let positionY = menuPosition.y;
  const menuWidth = 170;
  const subMenuWidth = 200;
  const menuHeight = 120;
  const subMenuHeight = 84;

  if( positionX + menuWidth >= windowWidth ) {
    positionX -= menuWidth;
  }
  if( positionX + menuWidth + subMenuWidth >= windowWidth ) {
    popup.classList.add('mode-left');
  }

  if(positionY + menuHeight > windowHeight) {
     positionY -= menuHeight;
  }
  if(positionY + menuHeight + subMenuHeight > windowHeight) {
     popup.classList.add('mode-top');
  }
   
  menuPositionX = positionX + 'px';
  menuPositionY = positionY + 'px';
   
  console.log('position-x: ' + menuPosition.x + 'position-y: ' + menuPosition.y);
  console.log('vmenu is: ' + vmenu);
   
  vmenu.style.left = menuPositionX;
  vmenu.style.top = menuPositionY;
}

var mouse_pos;


// DJC

function inactivateMenu ()
{
    menuState = 0;
    vmenu.classList.remove(active);

}


inactivateMenu ();

// document.addEventListener('click', inactivateMenu);

console.log ('Done running popup-menu-text...');     

"

	       #+nil
	       "
console.log ('running popup-menu-text...');     

function getPosition(e) {
var posx = 0;
var posy = 0;

if (!e) var e = window.event;

if (e.pageX || e.pageY) {
   posx = e.pageX;
   posy = e.pageY;
 } 
  else if (e.clientX || e.clientY) {
    posx = e.clientX + document.body.scrollLeft + 
                       document.documentElement.scrollLeft;
    posy = e.clientY + document.body.scrollTop + 
                       document.documentElement.scrollTop;
  }

  return {
    x: posx,
    y: posy
  }
}


var menuPosition;
var menuPositionX;
var menuPositionY;


var can = document.getElementById('x3dom-x3dom-1-canvas');
var  domid = document.getElementById('~a');

var obj = can || domid;

if (can) {

can.addEventListener('mousedown', function(e) {

 position = getPosition (e);

 if ((e.button == 0) && (e.altKey))
  {

   console.log(position);
   can.parent.doc.onDoubleClick(can.parent.gl, position.x, position.y);

   //    can.parent.onDoubleClick(e);
  }
 })
}


var menuState = 0;
var active = 'popup-menu-visible';
var vmenu = document.getElementById('~a');


function positionMenu(e) {
 
  let popup = document.querySelector('.popup-menu');
  popup.classList.remove('mode-left');
  popup.classList.remove('mode-top');
  menuPosition = getPosition(e);
  let windowWidth = window.innerWidth;
  let windowHeight = window.innerHeight;
  let positionX = menuPosition.x;
  let positionY = menuPosition.y;
  const menuWidth = 170;
  const subMenuWidth = 200;
  const menuHeight = 120;
  const subMenuHeight = 84;

  if( positionX + menuWidth >= windowWidth ) {
    positionX -= menuWidth;
  }
  if( positionX + menuWidth + subMenuWidth >= windowWidth ) {
    popup.classList.add('mode-left');
  }

  if(positionY + menuHeight > windowHeight) {
     positionY -= menuHeight;
  }
  if(positionY + menuHeight + subMenuHeight > windowHeight) {
     popup.classList.add('mode-top');
  }
   
  menuPositionX = positionX + 'px';
  menuPositionY = positionY + 'px';
   
  console.log('position-x: ' + menuPosition.x + 'position-y: ' + menuPosition.y);
  console.log('vmenu is: ' + vmenu);
   
  vmenu.style.left = menuPositionX;
  vmenu.style.top = menuPositionY;
}

var mouse_pos;


// DJC

if (obj)
{

obj.addEventListener('mousedown', function(e){
 mouse_pos = getPosition();
});



// DJC

obj.addEventListener('contextmenu', function(e){

e.preventDefault();
e.stopPropagation();

position = getPosition();


console.log('mouse_pos: x: ' + mouse_pos.x + ' y: ' + mouse_pos.y);
console.log('position: x: ' + position.x + ' y: ' + position.y);

  if ((menuState == 0) && (mouse_pos.x == position.x) && (mouse_pos.y == position.y)) {
    menuState = 1;
    vmenu.classList.add(active);
    makeMenuBarHoverable(e);
    positionMenu(e);
    vmenu.querySelector('li').classList.add('visible');
   }
   else
   {
     inactivateMenu();
//     menuState = 0;
//     vmenu.classList.remove(active);
   }



   console.log('menuState: ' + menuState);
})
}


function inactivateMenu ()
{
    menuState = 0;
    vmenu.classList.remove(active);

}


inactivateMenu ();

document.addEventListener('click', inactivateMenu);

console.log ('Done running popup-menu-text...');     

"
       (the dom-id)
       (the menu dom-id)))))

   
   (viewport-js-text
    (case (the image-format-selector value)
      (:x3dom
       (format nil "

function x3draw ()
{
 console.log('running x3draw for view-~(~a~)');

 if (x3dom.type != 'undefined') x3dom.reload(); 
 var elem = document.getElementById('view-~(~a~)');
 if (elem) elem.setAttribute('set_bind', 'true');
 var x3dom1 = document.getElementById('x3dom-1');
 var xruntime;
 if (x3dom1) xruntime= x3dom1.runtime;
 if (xruntime) xruntime.resetView();  // FLAG -- remember user specified view and set to that.
 if (xruntime) xruntime.fitAll(xruntime.getSceneBBox().min, xruntime.getSceneBBox().max);

 console.log('Done running x3draw');

 }

x3draw();


"  (the view-selector value) (the view-selector value)))
      (:svg
       "
 if (document.getElementById('svg-1'))
{
 panZoomSVG1 = svgPanZoom('#svg-1', {
 zoomEnabled: true,
 controlIconsEnabled: true,
 preventMouseEventsDefault: false,
 fit: true,
 minZoom: 0.01,
 maxZoom: 100,
 center: true});
};")))
   
   )
  
  
  
  :functions
  (
   ;;
   ;; FLAG -- consider changing back to list here and on add-node!, to
   ;; preserve ordering.
   ;;
   (add-leaves!
    (object) 
    
    (the (add-display-controls! object))
    (let ((hash (the view-object-roots-root-paths-hash))
	  (root-path (the-object object root-path)))
      (the (set-slot! :view-object-roots-root-paths-hash
		      (progn (unless (second (multiple-value-list (gethash root-path hash)))
			       (setf (gethash root-path hash) t)) hash))))
    #+nil
    (the (set-slot! :operations-list 
		    (append (the operations-list)
			    (list (list :operation :add-leaves :object object))))))

   #+nil
   (enter-debugger!
    ()
    
    (when (typep (the image-url) 'error)
      (let ((object (getf (lastcar (the operations-list)) :object)))
        (set-self object)
        (let ((*package* (symbol-package (the-object object root type))))
          
          (let ((no-trap? (member :notrap net.aserve::*debug-current*)))
            (unless no-trap? 
              (net.aserve::debug-on :notrap)
              (format t "~&Disabling AllegroServe error trapping (i.e. enabling debugging) 
  -- you can re-enable it with (net.aserve::debug-off :notrap)~%")
              (net.aserve::debug-on :notrap)))
          
          (break (the image-url))))))
   
   #+nil
   (pop-operation!
    ()
    (let ((item (lastcar (the operations-list))))
      (let ((operation (getf item :operation))
            (object (getf item :object)))
        (ecase operation
          (:add-leaves (the (delete-leaves! object)))
          (:add-leaves* (dolist (leaf (the-object object leaves))
                          (the (delete-leaves! leaf))))
          (:add-node (the (delete-node! object))))))
    (the (set-slot! :operations-list (butlast (the operations-list))))
    )

   
   
   (add-leaves*!
    (object)
    (the (add-display-controls! object))
    (dolist (leaf (the-object object leaves)) (the (add-node! leaf)))
    #+nil
    (the (set-slot! :operations-list 
                    (append (the operations-list)
                            (list (list :operation :add-leaves* :object object))))))

   (leaves-displayed? 
    (object)
    (gethash (the-object (first (the-object object leaves)) root-path)
	     (the view-objects-root-paths-hash)))

   #+nil
   (toggle-leaves*!
    (object)
    (if (the (leaves-displayed? object))
	(the (add-leaves*! object))
	(the (delete-leaves! object))))

   
   (delete-node!
    (object)
    (let ((display-controls-hash (the display-controls-root-path-hash))
          (root-path (the-object object root-path)))

      (the (clear-from-hash! :view-objects-root-paths-hash root-path))
      
      (the (set-slot! :display-controls-root-path-hash
                      (progn
                        (maphash #'(lambda(key val)
                                     (declare (ignore val))
                                     (when (and (<= (length root-path) (length key))
                                                (equalp (reverse root-path) (subseq (reverse key) 0 
                                                                                    (length root-path))))
                                       (remhash key display-controls-hash))) display-controls-hash) 
                        display-controls-hash)))))
   
   (delete-leaves!
    (object)
    (let ((display-controls-hash (the display-controls-root-path-hash))
          (root-path (the-object object root-path)))

      (dolist (hash-slot-key (list :view-object-roots-root-paths-hash
                                   :view-objects-root-paths-hash))
        (the (clear-from-hash! hash-slot-key root-path)))
      
      (the (set-slot! :display-controls-root-path-hash
                      (progn
                        (maphash #'(lambda(key val)
                                     (declare (ignore val))
                                     (when (and (<= (length root-path) (length key))
                                                (equalp (reverse root-path) (subseq (reverse key) 0 
                                                                                    (length root-path))))
                                       (remhash key display-controls-hash))) display-controls-hash) 
                        display-controls-hash)))))

   (clear-from-hash!
    (slot-key root-path)
    (let ((hash (the (evaluate slot-key))))
      (the (set-slot! slot-key
                      (progn
                        (maphash #'(lambda(key val)
                                     (declare (ignore val))
                                     (when (or (and (<= (length root-path) (length key))
                                                    (equalp (reverse root-path) 
                                                            (subseq (reverse key) 0 (length root-path)))))
                                       (remhash key hash))) hash) hash)))))
   
   (draw-leaves! 
    (object)     
    (the clear!) (the (add-leaves! object)))

   
   (add-node!
    (object)     
    (the (add-display-controls! object))
    (let ((hash (the view-objects-root-paths-hash))
          (root-path (the-object object root-path)))
      (the (set-slot! :view-objects-root-paths-hash
                      (progn (setf (gethash root-path hash) t) hash))))
    #+nil
    (the (set-slot! :operations-list 
                    (append (the operations-list)
                            (list (list :operation :add-node :object object))))))

   (draw-node! 
    (object)     
    (the clear!) (the (add-node! object)))
   
   (add-display-controls!
    (object)
    (let ((hash (the display-controls-root-path-hash))
          (root-path (the-object object root-path)))
      (the (set-slot! :display-controls-root-path-hash
                      (progn (let ((color (the color))
                                   (line-thickness (the line-thickness-selected)))
                               
                               (if (or color line-thickness)
                                   (setf (gethash root-path hash) 
					 (list :line-thickness line-thickness
					       :color (the color)))
				   (when (gethash root-path hash) (remhash root-path hash))))
                             hash)))))
   
   (clear! () 
           (the (clear-display-controls!)) (the (clear-view-object-roots!))
	   (the (clear-view-objects!)))
   
   (clear-display-controls!
    () (let ((hash (the display-controls-root-path-hash)))
         (the (set-slot! :display-controls-root-path-hash (progn (clrhash hash) hash)))))
   
   (clear-view-object-roots!
    () (let ((hash (the view-object-roots-root-paths-hash)))
         (the (set-slot! :view-object-roots-root-paths-hash (progn (clrhash hash) hash)))))
   
   (clear-view-objects!
    () (let ((hash (the view-objects-root-paths-hash)))
         (the (set-slot! :view-objects-root-paths-hash (progn (clrhash hash) hash)))))
   (set-object-to-inspect!
    (object)
    (the inspector (set-object! object)))))



(define-lens (html-format viewport)()
  :output-functions
  (

   (embedded-a-frame-world
    ()
    (with-cl-who ()
      (when (typep (the :view-object) 'null-part)
	(error "A valid :view-object of type web-drawing is required in the sheet 
to call the :embedded-a-frame-world output-function."))
	  
      (cond ((the no-graphics?)
	     (htm
	      ((:div :id "empty-viewport" :class "empty-viewport")
	       (str (the empty-display-list-message)))))
	    (t
	     (let ((*display-controls* (the display-controls-hash)))
	       (htm
                ((:a-scene :id "a-frame-1" :style "background: #b1b1b1"
                           ;;:width (if (numberp (the width)) (format nil "~apx" (the width)) (the width))
                           ;;:height (if (numberp (the length)) (format nil "~apx" (the length)) (the length))
                           )
                 (with-format (geom-base::a-frame *stream*)
                   (write-the view-object cad-output)))))))))
   
   #+nil
   ("Void. Writes an embedded a-scene tag and included content for the <tt>view-object</tt> child of this object. 
The <tt>view-object</tt> child should exist and be of type <tt>web-drawing</tt>."

    embedded-a-frame-world
    ()
    (with-cl-who ()
      (when (typep (the :view-object) 'null-part)
	(error "A valid :view-object of type web-drawing is required in the sheet 
to call the :embedded-a-frame-world output-function."))
	  
      (cond ((the no-graphics?)
	     (htm
	      ((:div :id "empty-viewport" :class "empty-viewport")
	       (str (the empty-display-list-message)))))
	    (t
	     (let ((*display-controls* (the display-controls-hash)))
	       (htm
                ((:a-scene :id "a-frame-1" :style "background: #b1b1b1"
                           :width (if (numberp (the width)) (format nil "~apx" (the width)) (the width))
                           :height (if (numberp (the length)) (format nil "~apx" (the length)) (the length)))
                 (with-format (geom-base::a-frame *stream*)
                   (write-the view-object cad-output)))))))))

   
   ("Void. Writes an embedded X3D tag and included content for the <tt>view-object</tt> child of this object. 
The <tt>view-object</tt> child should exist and be of type <tt>web-drawing</tt>."

    embedded-x3dom-world
    (&key (include-view-controls? nil))
    
    (declare (ignore include-view-controls?))
    
    ;; (the (restore-slot-default! :js-to-eval))
    
    (with-cl-who ()
      
      (when (typep (the :view-object) 'null-part)
	(error "A valid :view-object of type web-drawing is required in the sheet 
to call the :write-embedded-x3d-world function."))
	  
      (cond ((the no-graphics?)
	     (htm
	      ((:div :id "empty-viewport" :class "empty-viewport")
	       (str (the empty-display-list-message)))))
	    (t
	     (let ((*display-controls* (the display-controls-hash)))
	       (htm
		((:span :id "x3d-viewport")
		 ((:|X3D| :id "x3dom-1" :style "background: #d3d3d3"
		    :width (if (numberp (the width)) (format nil "~apx" (the width)) (the width))
		    :height (if (numberp (the length)) (format nil "~apx" (the length)) (the length)))
		  (:|Scene|
		     
		    ((:|navigationinfo| :|id| "navi" :|transitionTime| "0.0"
		       ))
		     
		     
		    (with-format (x3d *stream*) 
		      (let ((*onclick-function* (the onclick-function)))
			(write-the view-object cad-output)))))
		 
		 )
		))))))

   
   (inner-html
    ()
    
    (with-cl-who ()
      (let ((*display-controls* (the display-controls-hash)))
        (let ((image-format (if (the 2d-boxes?) (the image-format) :png)))
	  (ecase image-format
            (:a-frame (write-the embedded-a-frame-world))
            (:links (the (write-geometry-links)))
	    (:svg (write-the svg-vector-graphics))
            (:raphael (write-the vector-graphics))
            ((:png :jpeg) (write-the raster-graphics))
            (:x3dom (the (write-embedded-x3dom-world :include-view-controls? nil))))))))))








