
# Contributing

Genworks may offer AGPL license exception credits or other incentives
as consideration for code contributions (e.g. bugfixes, ports) made
via gitlab merge requests, under the terms of our Contributor's
Agreement. Please contact [Genworks](http://gen.works) for details.


