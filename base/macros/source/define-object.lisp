;;
;; Copyright 2012 Genworks International
;;
;; This source file is part of the General-purpose Declarative
;; Language project (GDL).
;;
;; This source file contains free software: you can redistribute it
;; and/or modify it under the terms of the GNU Affero General Public
;; License as published by the Free Software Foundation, either
;; version 3 of the License, or (at your option) any later version.
;; 
;; This source file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;; 
;; You should have received a copy of the GNU Affero General Public
;; License along with this source file.  If not, see
;; <http://www.gnu.org/licenses/>.
;; 
(in-package :gdl)


(defmacro define-object (name mixin-list &rest spec-plist)
  "Defines a standard GDL object. Please see the document USAGE.TXT for an 
overview of <tt>define-object</tt> syntax."
  `(%define-object% ,name ,mixin-list ,@(merge-common-keys spec-plist)))


(defmacro %define-object% (name mixin-list 
                           &key input-slots
                             computed-slots
                             objects
                             hidden-objects
                             functions
                             methods
                             (no-vanilla-mixin? nil)
                             documentation
                             trickle-down-slots
                             query-slots
                             )

  (check-syntax name input-slots computed-slots objects hidden-objects 
                functions methods documentation trickle-down-slots query-slots)

  (clear-metaclass-caches)

  (remhash name *reserved-words-hash*)
  
  (with-gdl-message-symbols (:new t)
    
    (let ((mixins (if (or no-vanilla-mixin? 
                          (some #'(lambda(mixin)
                                    (let ((class (find-class mixin nil)))
                                      (and class
                                           (member (find-class 'vanilla-mixin)
                                                   (all-superclasses class))))) mixin-list))
                      mixin-list (append mixin-list (list 'vanilla-mixin)))))
    
      (reserved-word-warning-or-error name messages mixins)

      
    
      (let ((message-ht (make-hash-table :size (length messages))))
        (dolist (message messages)
          (push message (gethash message message-ht)))
        (let ((duplicates (let (duplicates)
                            (maphash #'(lambda(key val)
                                         (if (consp (rest val)) 
                                             (push key duplicates))) message-ht) duplicates)))
          (let ((duplicates (remove-if #'(lambda(dup) (member dup method-syms)) duplicates)))
            (when duplicates
              (error "duplicate slot name~p: ~{~a~^, ~}" 
                     (length duplicates) (sort duplicates #'string-lessp :key #'symbol-name))))))

      `(progn 
         (defclass ,name ,mixins
           (,@(append (make-standard-slots) 
                      (make-accessible-slots computed-slot-syms)
                      (make-accessible-slots settable-computed-slot-syms)
                      (make-accessible-slots uncached-computed-slot-syms)
                      (make-accessible-slots query-slot-syms)                 
                      (make-accessible-slots object-syms) 
                      (make-accessible-slots quantified-object-syms)
                      (make-accessible-slots hidden-object-syms)
                      (make-accessible-slots quantified-hidden-object-syms)
                      (make-accessible-slots cached-function-syms)
                      (make-accessible-slots cached-method-syms)
                      (make-accessible-slots required-input-slot-syms :inputs? t)
                      (make-accessible-slots optional-input-slot-syms :inputs? t)
                      (make-accessible-slots settable-input-slot-syms :inputs? t)
                      (make-accessible-slots defaulted-input-slot-syms :inputs? t) 
                      (make-accessible-slots settable-defaulted-input-slot-syms :inputs? t)))  (:metaclass gdl-class))
       
         (let ((,class-arg (find-class ',name)))
         
           (let ((,old-message-keys (messages ,class-arg))
                 (,new-message-keys ',messages))
             (dolist (key (set-difference ,old-message-keys ,new-message-keys))
               (let ((method (ignore-errors
                               (find-method (symbol-function (glisp:intern (symbol-name key) :gdl-slots)) nil
                                            (list ,class-arg) nil))))
                 (when method 
                   (when *report-gdl-redefinitions?*
                     (format t "~%Removing slot: ~a for object definition: ~s~%" key ',name))
                   (remove-method (symbol-function (glisp:intern (symbol-name key) :gdl-slots)) method)))))
       
           ,(when (and *compile-documentation-database?* documentation)
                  `(when *load-documentation-database?*
                     (setf (gdl-documentation ,class-arg) ',documentation))))

         
         (when (message-documentation (find-class ',name)) (clrhash (message-documentation (find-class ',name))))
         (when (message-source (find-class ',name))(clrhash (message-source (find-class ',name))))
         (setf (messages (find-class ',name)) ',messages)
         (setf (required-input-slots (find-class ',name)) ',required-input-slot-syms)
         (setf (optional-input-slots (find-class ',name)) ',optional-input-slot-syms)
         (setf (defaulted-input-slots (find-class ',name))  ',defaulted-input-slot-syms)
         (setf (computed-slots (find-class ',name))  ',computed-slot-syms)
         (setf (query-slots (find-class ',name))  ',query-slot-syms)
         (setf (settable-computed-slots (find-class ',name)) ',settable-computed-slot-syms)
         (setf (uncached-computed-slots (find-class ',name)) ',uncached-computed-slot-syms)
         (setf (settable-optional-input-slots (find-class ',name)) ',settable-input-slot-syms)
         (setf (settable-defaulted-input-slots (find-class ',name)) ',settable-defaulted-input-slot-syms)
         (setf (functions (find-class ',name))  ',function-syms)
         (setf (methods (find-class ',name))  ',method-syms)
         (setf (cached-functions (find-class ',name))  ',cached-function-syms)
         (setf (cached-methods (find-class ',name))  ',cached-method-syms)
         (setf (objects (find-class ',name))  ',object-syms)
         (setf (quantified-objects (find-class ',name)) ',quantified-object-syms)
         (setf (hidden-objects (find-class ',name))  ',hidden-object-syms)
         (setf (quantified-hidden-objects (find-class ',name)) ',quantified-hidden-object-syms)
         (setf (trickle-down-slots (find-class ',name))
               ',(append object-syms quantified-object-syms hidden-object-syms quantified-hidden-object-syms trickle-down-slot-syms))
         (setf (settable-slots (find-class ',name)) ',(append required-input-slot-syms settable-input-slot-syms 
                                                              settable-defaulted-input-slot-syms settable-computed-slot-syms))
       
         ;; FLAG -- consider pre-cooking these expression lists
         ;;
       
       
         ,@(message-generics (set-difference messages (append method-syms cached-method-syms)))

         ,(input-slots-generics (append (group-remark-strings (remove-if-not #'(lambda(item)
                                                                                 (or (symbolp item) (stringp item)))
                                                                             input-slots))
                                        (remove-if-not #'consp input-slots)))

         ,@(input-slots-section name (group-remark-strings (remove-if-not #'(lambda(item)
                                                                              (or (symbolp item) (stringp item)))
                                                                          input-slots)))

         ,@(optional-input-slots-section 
            name (remove-if-not #'(lambda(slot) (and (consp slot) (null (rest (rest (strip-strings slot))))))
                                input-slots))

         ,@(optional-input-slots-section 
            name (remove-if-not 
                  #'(lambda(slot) 
                      (and (consp slot) 
                           (member :settable (rest (rest (strip-strings slot))))
                           (not (member :defaulting (rest (rest (strip-strings slot)))))))
                  input-slots))

         ,@(optional-input-slots-section 
            name (remove-if-not #'(lambda(slot) 
                                    (and (consp slot) 
                                         (member :defaulting (rest (rest (strip-strings slot))))
                                         (not (member :settable (rest (rest (strip-strings slot)))))))
                                input-slots) t)
       
         ,@(optional-input-slots-section
            name (remove-if-not 
                  #'(lambda(slot) 
                      (and (consp slot) 
                           (member :settable (rest (rest (strip-strings slot))))
                           (member :defaulting (rest (rest (strip-strings slot))))))
                  input-slots) t)
              
         ,@(computed-slots-section 
            name (remove-if-not #'(lambda(slot) (and (consp slot) (null (rest (rest (strip-strings slot))))))
                                computed-slots))
       
         ,@(computed-slots-section 
            name (remove-if-not #'(lambda(slot)
                                    (and (consp slot) (eql (first (rest (rest (strip-strings slot)))) 
                                                           :settable)))
                                computed-slots))
       
         ,@(computed-slots-section name query-slots :query? t)


       
         ,@(objects-section name (append objects hidden-objects))
       
       
         ,@(functions-section 
            name (mapcar #'(lambda(slot)
                             (if (stringp (first slot))
                                 (list (first slot) (second slot) nil (third slot))
                                 (list (first slot) nil (second slot))))
                         (remove-if-not #'(lambda(slot) (and (consp slot) (eql (first (rest (rest (strip-strings slot))))
                                                                               :uncached)))
                                        computed-slots)))
       
         ,@(functions-section name functions)
       
         ,@(methods-section name methods)

         ,@(trickle-down-slots-section trickle-down-slot-syms)

         ,@(trickle-down-slots-section (append object-syms quantified-object-syms hidden-object-syms quantified-hidden-object-syms)
                                       :from-objects? t)

	 ;;
	 ;; FLAG -- rewrite all of the below to use the metaclass slot
	 ;;         accessors rather than literally trying to collect
	 ;;         the lists of symbols here. That way it won't have
	 ;;         to be redefined for amendments.  As is it
	 ;;         currently, I think trickle-down-slots methods below
	 ;;         might be broken for amendments. 
	 ;;
	 #+nil
         (defmethod gdl-rule::%object-keywords%
             ((,self-arg ,name))
           (remove-duplicates
            (append ',object-syms ',quantified-object-syms
                    (when (next-method-p) (call-next-method)))))

	 ;;
	 ;; FLAG -- below is experimental
	 ;;
	 ;; FLAG put back a variant of this for vanilla-mixin* when we can test
	 #+nil
	 (defmethod gdl-rule::%object-keywords%
             ((,self-arg ,name))
	   (let ((class (find-class ',name)))
	     (or (objects-cache class)
		 (setf (objects-cache class)
		       (remove-duplicates
			(append (objects class) (quantified-objects class)
				(when (next-method-p) (call-next-method))))))))

	 #+nil
         (defmethod gdl-rule::%hidden-object-keywords%
             ((,self-arg ,name))
           (remove-duplicates
            (append ',hidden-object-syms ',quantified-hidden-object-syms
                    (when (next-method-p) (call-next-method)))))


	 #+nil ;; ditto for %object-keywords% above
	 (defmethod gdl-rule::%hidden-object-keywords%
             ((,self-arg ,name))
	   (let ((class (find-class ',name)))
	     (or (hidden-objects-cache class)
		 (setf (hidden-objects-cache class)
		       (remove-duplicates
			(append (hidden-objects class) (quantified-hidden-objects class)
				(when (next-method-p) (call-next-method))))))))

         ;;
         ;; FLAG -- below can probably go away.
         ;;

	 #+nil
         (let ((,self-arg (find-class ',name nil)))
           (when ,self-arg
             (setf (trickle-down-effective ,self-arg) nil)))

         ;;
         ;; FLAG -- use uiop:list-to-hash-set
         ;;
	 #+nil
         (defmethod gdl-rule::%trickle-down-slots% 
             ((,self-arg ,name))
           (let ((class (class-of ,self-arg)))
             (or (trickle-down-effective class)
                 (let* ((trickles (remove-duplicates
                                   (mapcar #'make-keyword
                                           (append ',(append object-syms quantified-object-syms hidden-object-syms 
                                                             quantified-hidden-object-syms trickle-down-slot-syms)
                                                   (when (next-method-p) (list-hash (call-next-method)))))))
                        (ht (glisp:make-sans-value-hash-table :size (length trickles))))
                   (dolist (trickle trickles ht)
                     (setf (gethash trickle ht) t))))))



	 #+nil
	 (defmethod gdl::try-next-method ((,self-arg ,name))

	   (print-variables ,self-arg ',name)
	   
	   (when (next-method-p)

	     (format t "calling next-method for ~a~%" ,self-arg)
	     
	     (call-next-method)))

	 
	 ;; FLAG -- figure out how to call these only once per class
	 ;; rather than instance. 
	 ;;
	 ;; FLAG -- write the following methods only once generically, no need
	 ;; to write with hardcoded names as below.
	 ;;
	 #+nil ;; this has been moved to a method in vanilla-mixin.lisp
	 (defmethod gdl-rule::%trickle-down-slots% 
             ((,self-arg ,name))
           (let ((class (find-class ',name)))
             (or (trickle-down-cache class)
		 (setf
		  (trickle-down-cache class)
                  (let* ((trickles (remove-duplicates
				    (mapcar #'make-keyword
					    (append (trickle-down-slots class)
						    (when (next-method-p)
						      (list-hash (call-next-method)))))))
                         (ht (glisp:make-sans-value-hash-table :size (length trickles))))
		    (dolist (trickle trickles ht) (setf (gethash trickle ht) t)))))))
	 

	 ;;
	 ;; FLAG -- all of the below methods should be out of here and written
	 ;; once in vanilla-mixin.lisp. 
	 ;;

	 
	 (defmethod gdl-rule::%settable-slot-list%
             ((,self-arg ,name))
	   (let ((class (find-class ',name)))
	     (or (settable-cache class)
		 (setf (settable-cache class)
		       (remove-duplicates
			(mapcar #'make-keyword
				(append (settable-slots (find-class ',name))
					(when (next-method-p) (call-next-method)))))))))
	 
	 
         (defmethod gdl-rule::%settable-slots% 
             ((,self-arg ,name))
           (let ((,ht-arg (glisp:make-sans-value-hash-table))
                 (,symbols-arg (gdl-rule::%settable-slot-list% ,self-arg))
                 (,non-settables-arg (mapcar #'make-keyword 
                                             (set-difference (messages (find-class ',name))
                                                             (settable-slots (find-class ',name))))))
             (dolist (,symbol-arg ,symbols-arg ,ht-arg) 
               (when (not (member ,symbol-arg ,non-settables-arg))
                 (setf (gethash ,symbol-arg ,ht-arg) t)))))
       
       
         (defmethod gdl-rule::%message-source%
             ((,self-arg ,name) ,message-arg &optional (,depth-arg 0))
           (let* ((method (nth ,depth-arg (compute-applicable-methods 
                                           #'gdl-rule::%message-source% (list ,self-arg ,message-arg))))
                  (class (first (glisp:gl-method-specializers method))))
             (append (let ((local (let ((ht (message-source class)))
                                    (when ht (gethash ,message-arg ht)))))
                       (when local (list (glisp:gl-class-name class) local)))
                     (when (next-method-p) (call-next-method ,self-arg ,message-arg (1+ ,depth-arg))))))
       

         (defmethod gdl-rule::%message-remarks%
             ((,self-arg ,name) ,message-arg &optional (,depth-arg 0))
           (let* ((method (nth ,depth-arg (compute-applicable-methods 
                                           #'gdl-rule::%message-remarks% (list ,self-arg ,message-arg))))
                  (class (first (glisp:gl-method-specializers method))))
             (append (let ((local (let ((ht (message-documentation class)))
                                    (when ht (gethash ,message-arg ht)))))
                       (when local (list (glisp:gl-class-name class) local)))
                     (when (next-method-p) (call-next-method ,self-arg ,message-arg (1+ ,depth-arg))))))

         ;;
         ;; FLAG -- why is this hardcoded to :all when it is first called?
         ;;
         (defmethod gdl-rule::%message-list%
           ((,self-arg ,name) ,category-arg ,message-type-arg ,base-part-type-arg
            &optional (,depth-arg 0))
         (append (message-list ,self-arg 
                               ;;:all 
                               ,category-arg
                               ,depth-arg ,base-part-type-arg)
                 (when (and (not (eql ,message-type-arg :local)) (next-method-p))
                   (call-next-method ,self-arg ,category-arg ,message-type-arg ,base-part-type-arg (1+ ,depth-arg)))))


	 (eval-when (:load-toplevel)
	     (let ((class (find-class ',name)))
	       (dolist (super (cons class (all-superclasses class)))
		 (let ((ht (or (gethash super *class-used-by-hash*)
			       (setf (gethash super *class-used-by-hash*)
				     (glisp:make-sans-value-hash-table)))))
		   (setf (gethash class ht) t)))
	       (maphash #'(lambda(key value)
			    (declare (ignore value))
			    (setf (gethash key *message-list-hashes*) nil))
			(gethash class *class-used-by-hash*))))

       
	 (clear-metaclass-caches)
	 
	 (find-class ',name)))))




      
